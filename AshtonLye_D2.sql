--Deliverable 2 - Databases - Ashton Lye 9997847

--DROP DATABASE DRIVING_SCHOOL
--CREATE DATABASE DRIVING_SCHOOL

--USE DRIVING_SCHOOL

--EXEC sp_droptype NAME
--EXEC sp_droptype LOGIN
--GO

--EXEC sp_addtype NAME, 'VARCHAR(30)','NOT NULL'
--EXEC sp_addtype LOGIN, 'VARCHAR(20)','NOT NULL'
--GO

CREATE TABLE Admin (
admin_username LOGIN PRIMARY KEY,
password LOGIN,
sname NAME,
fname NAME,
email VARCHAR(30) NOT NULL,
)

CREATE TABLE Instructor (
instructor_username LOGIN PRIMARY KEY,
password LOGIN,
sname NAME,
fname NAME,
email VARCHAR(30) NOT NULL,
phone INT NOT NULL,
)

CREATE TABLE Type (
type_name VARCHAR(20) PRIMARY KEY,
cost INT NOT NULL,
hours INT NOT NULL,
)

CREATE TABLE Document (
doc_id INT PRIMARY KEY,
date DATE NOT NULL,
link VARCHAR(50) NOT NULL,
doc_type VARCHAR(30) NOT NULL,
)

CREATE TABLE Car (
licence VARCHAR(6) PRIMARY KEY,
make VARCHAR(20)
)

CREATE TABLE Client (
client_username LOGIN PRIMARY KEY,
password LOGIN,
sname NAME,
fname NAME,
email VARCHAR(30) NOT NULL,
phone INT NOT NULL,
type_name VARCHAR(20) NOT NULL,
doc_id INT,
FOREIGN KEY (type_name) REFERENCES Type,
FOREIGN KEY (doc_id) REFERENCES Document,
)

CREATE TABLE Appointment (
id INT PRIMARY KEY,
notes VARCHAR(140),
appt_time VARCHAR(50) NOT NULL,
appt_date DATE NOT NULL,
client_username LOGIN,
instructor_username LOGIN,
FOREIGN KEY (client_username)	REFERENCES Client,
FOREIGN KEY (instructor_username) REFERENCES Instructor,
)

CREATE TABLE Leave (
id INT PRIMARY KEY,
leave_time VARCHAR(50) NOT NULL,
leave_date DATE NOT NULL,
instructor_username LOGIN,
FOREIGN KEY (instructor_username) REFERENCES Instructor,
)

CREATE TABLE Assigned(
instructor_username LOGIN NOT NULL,
confirmed VARCHAR(1) NOT NULL,
licence VARCHAR(6) NOT NULL,
id INT NOT NULL,
FOREIGN KEY (instructor_username) REFERENCES Instructor,
FOREIGN KEY (licence) REFERENCES Car,
FOREIGN KEY (id) REFERENCES Appointment,
)

INSERT INTO Instructor VALUES ('jhalpert','jim123', 'Halpert', 'Jim', 'jhalpert@drivingschool.com', 0211234567)
INSERT INTO Instructor VALUES ('dschrute','dwight123','Schrute','Dwight','dschrute@drivingschool.com', 0223217654)
INSERT INTO Instructor VALUES ('abernard','andy123','Bernard','Andy','abernard@drivingschool.com', 0279876543)
INSERT INTO Instructor VALUES ('pvance','phyllis123','Vance','Phyllis','pvance@drivingschool.com', 0214563245)

INSERT INTO Admin VALUES ('phalpert','pam123','Halpert','Pam','phalpert@drivingschool.com')
INSERT INTO Admin VALUES ('mscott','michael123','Scott','Michael','mscott@drivingschool.com')
INSERT INTO Admin VALUES ('ehannon','erin123','Hannon','Erin','ehannon@drivingschool.com')

INSERT INTO Car VALUES ('ABC123','Toyota')
INSERT INTO Car VALUES ('DEF456','Honda')
INSERT INTO Car VALUES ('GHI789','Ford')
INSERT INTO Car VALUES ('JKL987','Holden')
INSERT INTO Car VALUES ('MNO654','Nissan')

INSERT INTO Type VALUES ('experienced',100,2)
INSERT INTO Type VALUES ('new',200,5)

INSERT INTO Client VALUES ('ashton', 'ashton', 'Lye', 'Ashton', 'atticuslye@gmail.com', 0223247731, 'Experienced', NULL )

SELECT * FROM Admin
SELECT * FROM Instructor
SELECT * FROM Type
SELECT * FROM Document
SELECT * FROM Car
SELECT * FROM Client
SELECT * FROM Appointment
SELECT * FROM Assigned
SELECT * FROM Leave


--DROP TABLE Appointment
--DROP TABLE Client
--DROP TABLE Assigned
--DROP TABLE Admin
--DROP TABLE Instructor
--DROP TABLE Type
--DROP TABLE Document
--DROP TABLE Car
--DROP TABLE Leave
