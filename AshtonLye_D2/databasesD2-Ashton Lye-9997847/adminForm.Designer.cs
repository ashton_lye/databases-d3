﻿namespace databasesD2_Ashton_Lye_9997847
{
    partial class adminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBoxAdminPassword = new System.Windows.Forms.TextBox();
            this.adminBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.adminDataSet = new databasesD2_Ashton_Lye_9997847.adminDataSet();
            this.textBoxAdminUsername = new System.Windows.Forms.TextBox();
            this.buttonAdminBack = new System.Windows.Forms.Button();
            this.buttonAdminLogin = new System.Windows.Forms.Button();
            this.labelClientPassword = new System.Windows.Forms.Label();
            this.labelClientUsername = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.titleLabel = new System.Windows.Forms.Label();
            this.adminTableAdapter = new databasesD2_Ashton_Lye_9997847.adminDataSetTableAdapters.AdminTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.adminBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adminDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxAdminPassword
            // 
            this.textBoxAdminPassword.Location = new System.Drawing.Point(368, 195);
            this.textBoxAdminPassword.Name = "textBoxAdminPassword";
            this.textBoxAdminPassword.Size = new System.Drawing.Size(139, 20);
            this.textBoxAdminPassword.TabIndex = 23;
            this.textBoxAdminPassword.UseSystemPasswordChar = true;
            // 
            // adminBindingSource
            // 
            this.adminBindingSource.DataMember = "Admin";
            this.adminBindingSource.DataSource = this.adminDataSet;
            // 
            // adminDataSet
            // 
            this.adminDataSet.DataSetName = "adminDataSet";
            this.adminDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // textBoxAdminUsername
            // 
            this.textBoxAdminUsername.Location = new System.Drawing.Point(368, 153);
            this.textBoxAdminUsername.Name = "textBoxAdminUsername";
            this.textBoxAdminUsername.Size = new System.Drawing.Size(139, 20);
            this.textBoxAdminUsername.TabIndex = 21;
            this.textBoxAdminUsername.TextChanged += new System.EventHandler(this.textBoxAdminUsername_TextChanged);
            // 
            // buttonAdminBack
            // 
            this.buttonAdminBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdminBack.Location = new System.Drawing.Point(368, 352);
            this.buttonAdminBack.Name = "buttonAdminBack";
            this.buttonAdminBack.Size = new System.Drawing.Size(139, 39);
            this.buttonAdminBack.TabIndex = 26;
            this.buttonAdminBack.Text = "Back";
            this.buttonAdminBack.UseVisualStyleBackColor = true;
            this.buttonAdminBack.Click += new System.EventHandler(this.buttonAdminBack_Click);
            // 
            // buttonAdminLogin
            // 
            this.buttonAdminLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdminLogin.Location = new System.Drawing.Point(368, 238);
            this.buttonAdminLogin.Name = "buttonAdminLogin";
            this.buttonAdminLogin.Size = new System.Drawing.Size(139, 39);
            this.buttonAdminLogin.TabIndex = 25;
            this.buttonAdminLogin.Text = "Login";
            this.buttonAdminLogin.UseVisualStyleBackColor = true;
            this.buttonAdminLogin.Click += new System.EventHandler(this.buttonAdminLogin_Click);
            // 
            // labelClientPassword
            // 
            this.labelClientPassword.AutoSize = true;
            this.labelClientPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelClientPassword.Location = new System.Drawing.Point(275, 195);
            this.labelClientPassword.Name = "labelClientPassword";
            this.labelClientPassword.Size = new System.Drawing.Size(82, 20);
            this.labelClientPassword.TabIndex = 24;
            this.labelClientPassword.Text = "Password:";
            // 
            // labelClientUsername
            // 
            this.labelClientUsername.AutoSize = true;
            this.labelClientUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelClientUsername.Location = new System.Drawing.Point(275, 153);
            this.labelClientUsername.Name = "labelClientUsername";
            this.labelClientUsername.Size = new System.Drawing.Size(87, 20);
            this.labelClientUsername.TabIndex = 22;
            this.labelClientUsername.Text = "Username:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(338, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 33);
            this.label1.TabIndex = 20;
            this.label1.Text = "Admin Login";
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.Location = new System.Drawing.Point(97, 10);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(690, 67);
            this.titleLabel.TabIndex = 27;
            this.titleLabel.Text = "Driving Instruction Academy";
            // 
            // adminTableAdapter
            // 
            this.adminTableAdapter.ClearBeforeFill = true;
            // 
            // adminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Brown;
            this.ClientSize = new System.Drawing.Size(884, 461);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.buttonAdminBack);
            this.Controls.Add(this.buttonAdminLogin);
            this.Controls.Add(this.labelClientPassword);
            this.Controls.Add(this.textBoxAdminPassword);
            this.Controls.Add(this.labelClientUsername);
            this.Controls.Add(this.textBoxAdminUsername);
            this.Controls.Add(this.label1);
            this.Name = "adminForm";
            this.Text = "adminForm";
            this.Load += new System.EventHandler(this.adminForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.adminBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adminDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAdminBack;
        private System.Windows.Forms.Button buttonAdminLogin;
        private System.Windows.Forms.Label labelClientPassword;
        private System.Windows.Forms.Label labelClientUsername;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label titleLabel;
        private adminDataSet adminDataSet;
        private System.Windows.Forms.BindingSource adminBindingSource;
        private adminDataSetTableAdapters.AdminTableAdapter adminTableAdapter;
        private System.Windows.Forms.TextBox textBoxAdminPassword;
        private System.Windows.Forms.TextBox textBoxAdminUsername;
    }
}