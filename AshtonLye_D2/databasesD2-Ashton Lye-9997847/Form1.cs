﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace databasesD2_Ashton_Lye_9997847
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'clientDataSet.Client' table. You can move, or remove it, as needed.
            this.clientTableAdapter.Fill(this.clientDataSet.Client);
            // TODO: This line of code loads data into the 'adminDataSet.Admin' table. You can move, or remove it, as needed.
            this.adminTableAdapter.Fill(this.adminDataSet.Admin);
        }

        private void buttonClient_Click(object sender, EventArgs e)
        {
            clientForm clientForm1 = new clientForm();
            clientForm1.Show();
        }

        private void buttonInstructor_Click(object sender, EventArgs e)
        {
            instructorForm instructorForm1 = new instructorForm();
            instructorForm1.Show();
        }

        private void buttonAdmin_Click(object sender, EventArgs e)
        {
            adminForm adminForm1 = new adminForm();
            adminForm1.Show();
        }

        private void buttonRegister_Click(object sender, EventArgs e)
        {
            registerForm registerForm1 = new registerForm();
            registerForm1.Show();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
