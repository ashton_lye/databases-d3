﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace databasesD2_Ashton_Lye_9997847
{
    public partial class instructorAvailability : Form
    {

        public instructorAvailability(string userName, string origin)
        {
            InitializeComponent();

            if (origin == "instructorFunctions")
            {
                comboBoxInstructor.Hide();
                comboBoxInstructor.Text = userName;
                instructorNameLabel.Show();
                instructorNameLabel.Text = userName;
            }
        }

        private void bookLeaveButton_Click(object sender, EventArgs e)
        {
            //bookLeave();

            var leaveCount = leaveDataSet.Leave.Rows.Count;
            var count = leaveCount;

            var time = listBoxTime.Text;
            var date = dateTimePickerDate.Text;
            var uname = comboBoxInstructor.Text;             

            //check if leave is already booked
            leaveTableAdapter.CheckLeaveBooked(leaveDataSet.Leave, time, date, uname);
            if (leaveDataSet.Leave.Rows.Count == 1)
            {
                MessageBox.Show("Sorry, you cannot book Leave at that time. Please Try Booking at Another Time or Date.");
            }
            else
            {
                try
                {
                    //create new session
                    DataRow newLeaveRow = leaveDataSet.Tables["Leave"].NewRow();

                    newLeaveRow["id"] = leaveCount + 1;
                    newLeaveRow["leave_time"] = time;
                    newLeaveRow["leave_date"] = date;
                    newLeaveRow["instructor_username"] = uname;

                    leaveDataSet.Tables["Leave"].Rows.Add(newLeaveRow);
                    leaveTableAdapter.Update(leaveDataSet);
                    count++;

                    instructorAvailability_Load(sender, e);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Please enter valid information");
                }

                if (count > leaveCount)
                {
                    MessageBox.Show("Leave Booked Successfully!");
                }
                else
                {
                    MessageBox.Show("Booking Unsuccessful :(");
                    instructorAvailability_Load(sender, e);
                }

            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void instructorAvailability_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'instructorDataSet1.Instructor' table. You can move, or remove it, as needed.
            this.instructorTableAdapter.Fill(this.instructorDataSet1.Instructor);
            // TODO: This line of code loads data into the 'leaveDataSet.Leave' table. You can move, or remove it, as needed.
            this.leaveTableAdapter.Fill(this.leaveDataSet.Leave);

        }

    }

}

