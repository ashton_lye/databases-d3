﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace databasesD2_Ashton_Lye_9997847
{
    public partial class bookSession : Form
    {
        private static string us1;

        public bookSession(string userName)
        {
            InitializeComponent();
            us1 = userName;

            comboBoxInstructor.SelectedText = us1;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void bookSession_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'leaveDataSet.Leave' table. You can move, or remove it, as needed.
            this.leaveTableAdapter.Fill(this.leaveDataSet.Leave);
            // TODO: This line of code loads data into the 'appointmentDataSet.Appointment' table. You can move, or remove it, as needed.
            this.appointmentTableAdapter.Fill(this.appointmentDataSet.Appointment);
            // TODO: This line of code loads data into the 'instructorDataSet1.Instructor' table. You can move, or remove it, as needed.
            this.instructorTableAdapter.Fill(this.instructorDataSet1.Instructor);
            // TODO: This line of code loads data into the 'adminDataSet.Admin' table. You can move, or remove it, as needed.
            this.adminTableAdapter.Fill(this.adminDataSet.Admin);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //addSession(us1);

            var sessions = appointmentDataSet.Appointment.Rows.Count;
            var count = sessions;

            var time = listBoxTime.Text;
            var date = dateTimePickerDate.Text;
            var uname = instructorTableAdapter.GetUsername(comboBoxInstructor.Text);

            //check if a session is already booked
            appointmentTableAdapter.CheckSessionBooked(appointmentDataSet.Appointment, time, date, uname);
            //check if the instructor is on leave
            leaveTableAdapter.CheckLeaveBooked(leaveDataSet.Leave, time, date, uname);
            if (appointmentDataSet.Appointment.Rows.Count == 1 || leaveDataSet.Leave.Rows.Count == 1 || comboBoxInstructor.Text == "Spare")
            {
                MessageBox.Show("Sorry, A Session is not Available at that Time and Date with that Instructor. Please Try Booking with Another Instructor or at a Different Time or Date.");
            }
            else
            {
                try
                {
                    //create new session
                    DataRow newAppointmentRow = appointmentDataSet.Tables["Appointment"].NewRow();

                    newAppointmentRow["id"] = sessions + 1;
                    newAppointmentRow["appt_time"] = time;
                    newAppointmentRow["appt_date"] = date;
                    newAppointmentRow["instructor_username"] = uname;
                    newAppointmentRow["client_username"] = us1;

                    appointmentDataSet.Tables["Appointment"].Rows.Add(newAppointmentRow);
                    appointmentTableAdapter.Update(appointmentDataSet);
                    count++;

                    bookSession_Load(sender, e);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Please enter valid information");
                }

                if (count > sessions)
                {
                    MessageBox.Show("Session Booked Successfully!");
                }
                else
                {
                    MessageBox.Show("Booking Unsuccessful :(");
                }
            }
        }

        private void comboBoxInstructor_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
