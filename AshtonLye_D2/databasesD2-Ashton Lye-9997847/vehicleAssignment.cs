﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace databasesD2_Ashton_Lye_9997847
{
    public partial class vehicleAssignment : Form
    {
        public vehicleAssignment()
        {
            InitializeComponent();
        }

        private void vehicleAssignment_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'assignedDataSet.Assigned' table. You can move, or remove it, as needed.
            this.assignedTableAdapter.Fill(this.assignedDataSet.Assigned);
            // TODO: This line of code loads data into the 'vehicleDataSet.Car' table. You can move, or remove it, as needed.
            this.carTableAdapter.Fill(this.vehicleDataSet.Car);
            // TODO: This line of code loads data into the 'instructorDataSet1.Instructor' table. You can move, or remove it, as needed.
            this.instructorTableAdapter.Fill(this.instructorDataSet1.Instructor);

        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonAssign_Click(object sender, EventArgs e)
        {
            var instructor = comboBoxInstructor.Text;
            var vehicle = comboBoxVehicle.Text;

            var assignments = assignedDataSet.Assigned.Rows.Count;
            var count = assignments;

            assignedTableAdapter.CheckVehicleAssigned(assignedDataSet.Assigned, vehicle, instructor);
            if (assignedDataSet.Assigned.Rows.Count == 1)
            {
                MessageBox.Show("Vehicle already assigned! Please assign a different vehicle.");
            }
            else
            {
                try
                {
                    DataRow newAssignmentRow = assignedDataSet.Tables["Assigned"].NewRow();

                    newAssignmentRow["id"] = assignments + 1;
                    newAssignmentRow["instructor_username"] = instructor;
                    newAssignmentRow["licence"] = vehicle;


                    assignedDataSet.Tables["Assigned"].Rows.Add(newAssignmentRow);
                    assignedTableAdapter.Update(assignedDataSet);
                    count++;

                    vehicleAssignment_Load(sender, e);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Please enter valid information");
                }

                if (count > assignments)
                {
                    MessageBox.Show("Vehicle Assigned Successfully!");
                }
                else
                {
                    MessageBox.Show("Assignment Unsuccessful :(");
                }
            }
        }
    }
}
