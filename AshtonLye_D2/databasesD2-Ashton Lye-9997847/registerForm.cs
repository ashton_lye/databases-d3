﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace databasesD2_Ashton_Lye_9997847
{
    public partial class registerForm : Form
    {
        public registerForm()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void buttonRegisterBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonRegister_Click(object sender, EventArgs e)
        {
            var clients = clientDataSet.Client.Rows.Count;
            var count = clients;

            try
            {
                DataRow newClientRow = clientDataSet.Tables["Client"].NewRow();

                newClientRow["client_username"] = client_usernameTextBox.Text;
                newClientRow["password"] = passwordTextBox.Text;
                newClientRow["sname"] = snameTextBox.Text;
                newClientRow["fname"] = fnameTextBox.Text;
                newClientRow["email"] = emailTextBox.Text;
                newClientRow["phone"] = phoneTextBox.Text;
                newClientRow["type_name"] = type_nameComboBox.Text;

                clientDataSet.Tables["Client"].Rows.Add(newClientRow); clientTableAdapter.Update(clientDataSet);
                count++;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please enter valid information - E.G no letters in your phone number.");
            }

            if (count > clients)
            {
                MessageBox.Show("Client Registered Successfully!");
            }
            else
            {
                MessageBox.Show("Registration Unsuccessful :(");
            }
        }

        private void clientBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.clientBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.clientDataSet);

        }

        private void registerForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'clientDataSet.Client' table. You can move, or remove it, as needed.
            this.clientTableAdapter.Fill(this.clientDataSet.Client);

        }

        private void passwordLabel_Click(object sender, EventArgs e)
        {

        }

        private void snameLabel_Click(object sender, EventArgs e)
        {

        }

        private void fnameLabel_Click(object sender, EventArgs e)
        {

        }

        private void emailLabel_Click(object sender, EventArgs e)
        {

        }

        private void phoneLabel_Click(object sender, EventArgs e)
        {

        }

        private void type_nameLabel_Click(object sender, EventArgs e)
        {

        }

        private void titleLabel_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void client_usernameLabel_Click(object sender, EventArgs e)
        {

        }

        private void client_usernameTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void passwordTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void clientBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void snameTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void fnameTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void emailTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void phoneTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void type_nameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
