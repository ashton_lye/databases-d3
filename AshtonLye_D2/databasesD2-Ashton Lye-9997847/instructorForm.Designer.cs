﻿namespace databasesD2_Ashton_Lye_9997847
{
    partial class instructorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.titleLabel = new System.Windows.Forms.Label();
            this.buttonInstructorBack = new System.Windows.Forms.Button();
            this.buttonInstructorLogin = new System.Windows.Forms.Button();
            this.labelClientPassword = new System.Windows.Forms.Label();
            this.textBoxInstructorPassword = new System.Windows.Forms.TextBox();
            this.labelClientUsername = new System.Windows.Forms.Label();
            this.textBoxInstructorUsername = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.instructorBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.instructorDataSet = new databasesD2_Ashton_Lye_9997847.instructorDataSet1();
            this.instructorTableAdapter = new databasesD2_Ashton_Lye_9997847.instructorDataSet1TableAdapters.InstructorTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.instructorBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instructorDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.Location = new System.Drawing.Point(97, 10);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(690, 67);
            this.titleLabel.TabIndex = 1;
            this.titleLabel.Text = "Driving Instruction Academy";
            // 
            // buttonInstructorBack
            // 
            this.buttonInstructorBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonInstructorBack.Location = new System.Drawing.Point(370, 354);
            this.buttonInstructorBack.Name = "buttonInstructorBack";
            this.buttonInstructorBack.Size = new System.Drawing.Size(139, 39);
            this.buttonInstructorBack.TabIndex = 19;
            this.buttonInstructorBack.Text = "Back";
            this.buttonInstructorBack.UseVisualStyleBackColor = true;
            this.buttonInstructorBack.Click += new System.EventHandler(this.buttonInstructorBack_Click);
            // 
            // buttonInstructorLogin
            // 
            this.buttonInstructorLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonInstructorLogin.Location = new System.Drawing.Point(370, 240);
            this.buttonInstructorLogin.Name = "buttonInstructorLogin";
            this.buttonInstructorLogin.Size = new System.Drawing.Size(139, 39);
            this.buttonInstructorLogin.TabIndex = 17;
            this.buttonInstructorLogin.Text = "Login";
            this.buttonInstructorLogin.UseVisualStyleBackColor = true;
            this.buttonInstructorLogin.Click += new System.EventHandler(this.buttonInstructorLogin_Click);
            // 
            // labelClientPassword
            // 
            this.labelClientPassword.AutoSize = true;
            this.labelClientPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelClientPassword.Location = new System.Drawing.Point(277, 197);
            this.labelClientPassword.Name = "labelClientPassword";
            this.labelClientPassword.Size = new System.Drawing.Size(82, 20);
            this.labelClientPassword.TabIndex = 16;
            this.labelClientPassword.Text = "Password:";
            // 
            // textBoxInstructorPassword
            // 
            this.textBoxInstructorPassword.Location = new System.Drawing.Point(370, 197);
            this.textBoxInstructorPassword.Name = "textBoxInstructorPassword";
            this.textBoxInstructorPassword.Size = new System.Drawing.Size(139, 20);
            this.textBoxInstructorPassword.TabIndex = 15;
            this.textBoxInstructorPassword.UseSystemPasswordChar = true;
            this.textBoxInstructorPassword.TextChanged += new System.EventHandler(this.textBoxInstructorPassword_TextChanged);
            // 
            // labelClientUsername
            // 
            this.labelClientUsername.AutoSize = true;
            this.labelClientUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelClientUsername.Location = new System.Drawing.Point(277, 155);
            this.labelClientUsername.Name = "labelClientUsername";
            this.labelClientUsername.Size = new System.Drawing.Size(87, 20);
            this.labelClientUsername.TabIndex = 14;
            this.labelClientUsername.Text = "Username:";
            // 
            // textBoxInstructorUsername
            // 
            this.textBoxInstructorUsername.Location = new System.Drawing.Point(370, 155);
            this.textBoxInstructorUsername.Name = "textBoxInstructorUsername";
            this.textBoxInstructorUsername.Size = new System.Drawing.Size(139, 20);
            this.textBoxInstructorUsername.TabIndex = 13;
            this.textBoxInstructorUsername.TextChanged += new System.EventHandler(this.textBoxInstructorUsername_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(340, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(198, 33);
            this.label1.TabIndex = 12;
            this.label1.Text = "Instructor Login";
            // 
            // instructorBindingSource
            // 
            this.instructorBindingSource.DataMember = "Instructor";
            this.instructorBindingSource.DataSource = this.instructorDataSet;
            // 
            // instructorDataSet
            // 
            this.instructorDataSet.DataSetName = "instructorDataSet";
            this.instructorDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // instructorTableAdapter
            // 
            this.instructorTableAdapter.ClearBeforeFill = true;
            // 
            // instructorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Brown;
            this.ClientSize = new System.Drawing.Size(884, 461);
            this.Controls.Add(this.buttonInstructorBack);
            this.Controls.Add(this.buttonInstructorLogin);
            this.Controls.Add(this.labelClientPassword);
            this.Controls.Add(this.textBoxInstructorPassword);
            this.Controls.Add(this.labelClientUsername);
            this.Controls.Add(this.textBoxInstructorUsername);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.titleLabel);
            this.Name = "instructorForm";
            this.Text = "instructorForm";
            this.Load += new System.EventHandler(this.instructorForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.instructorBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instructorDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Button buttonInstructorBack;
        private System.Windows.Forms.Button buttonInstructorLogin;
        private System.Windows.Forms.Label labelClientPassword;
        private System.Windows.Forms.Label labelClientUsername;
        private System.Windows.Forms.Label label1;
        private instructorDataSet1 instructorDataSet;
        private System.Windows.Forms.BindingSource instructorBindingSource;
        private instructorDataSet1TableAdapters.InstructorTableAdapter instructorTableAdapter;
        private System.Windows.Forms.TextBox textBoxInstructorPassword;
        private System.Windows.Forms.TextBox textBoxInstructorUsername;
    }
}