﻿namespace databasesD2_Ashton_Lye_9997847
{
    partial class vehicleAssignment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.titleLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxInstructor = new System.Windows.Forms.ComboBox();
            this.instructorBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.instructorDataSet1 = new databasesD2_Ashton_Lye_9997847.instructorDataSet1();
            this.instructorTableAdapter = new databasesD2_Ashton_Lye_9997847.instructorDataSet1TableAdapters.InstructorTableAdapter();
            this.comboBoxVehicle = new System.Windows.Forms.ComboBox();
            this.carBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vehicleDataSet = new databasesD2_Ashton_Lye_9997847.vehicleDataSet();
            this.carTableAdapter = new databasesD2_Ashton_Lye_9997847.vehicleDataSetTableAdapters.CarTableAdapter();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonAssign = new System.Windows.Forms.Button();
            this.buttonBack = new System.Windows.Forms.Button();
            this.listBoxInstructor = new System.Windows.Forms.ListBox();
            this.assignedBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.assignedDataSet = new databasesD2_Ashton_Lye_9997847.assignedDataSet();
            this.label4 = new System.Windows.Forms.Label();
            this.listBoxLicence = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.assignedTableAdapter = new databasesD2_Ashton_Lye_9997847.assignedDataSetTableAdapters.AssignedTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.instructorBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instructorDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.carBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.assignedBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.assignedDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.Location = new System.Drawing.Point(97, 10);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(690, 67);
            this.titleLabel.TabIndex = 1;
            this.titleLabel.Text = "Driving Instruction Academy";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(293, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(244, 33);
            this.label1.TabIndex = 2;
            this.label1.Text = "Vehicle Assignments";
            // 
            // comboBoxInstructor
            // 
            this.comboBoxInstructor.DataSource = this.instructorBindingSource;
            this.comboBoxInstructor.DisplayMember = "instructor_username";
            this.comboBoxInstructor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxInstructor.FormattingEnabled = true;
            this.comboBoxInstructor.Location = new System.Drawing.Point(109, 170);
            this.comboBoxInstructor.Name = "comboBoxInstructor";
            this.comboBoxInstructor.Size = new System.Drawing.Size(162, 28);
            this.comboBoxInstructor.TabIndex = 3;
            this.comboBoxInstructor.ValueMember = "instructor_username";
            // 
            // instructorBindingSource
            // 
            this.instructorBindingSource.DataMember = "Instructor";
            this.instructorBindingSource.DataSource = this.instructorDataSet1;
            // 
            // instructorDataSet1
            // 
            this.instructorDataSet1.DataSetName = "instructorDataSet1";
            this.instructorDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // instructorTableAdapter
            // 
            this.instructorTableAdapter.ClearBeforeFill = true;
            // 
            // comboBoxVehicle
            // 
            this.comboBoxVehicle.DataSource = this.carBindingSource;
            this.comboBoxVehicle.DisplayMember = "licence";
            this.comboBoxVehicle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxVehicle.FormattingEnabled = true;
            this.comboBoxVehicle.Location = new System.Drawing.Point(109, 224);
            this.comboBoxVehicle.Name = "comboBoxVehicle";
            this.comboBoxVehicle.Size = new System.Drawing.Size(162, 28);
            this.comboBoxVehicle.TabIndex = 4;
            this.comboBoxVehicle.ValueMember = "licence";
            // 
            // carBindingSource
            // 
            this.carBindingSource.DataMember = "Car";
            this.carBindingSource.DataSource = this.vehicleDataSet;
            // 
            // vehicleDataSet
            // 
            this.vehicleDataSet.DataSetName = "vehicleDataSet";
            this.vehicleDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // carTableAdapter
            // 
            this.carTableAdapter.ClearBeforeFill = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(105, 147);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Instructor";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(105, 201);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Vehicle";
            // 
            // buttonAssign
            // 
            this.buttonAssign.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAssign.Location = new System.Drawing.Point(109, 273);
            this.buttonAssign.Name = "buttonAssign";
            this.buttonAssign.Size = new System.Drawing.Size(162, 42);
            this.buttonAssign.TabIndex = 7;
            this.buttonAssign.Text = "Assign Vehicle";
            this.buttonAssign.UseVisualStyleBackColor = true;
            this.buttonAssign.Click += new System.EventHandler(this.buttonAssign_Click);
            // 
            // buttonBack
            // 
            this.buttonBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBack.Location = new System.Drawing.Point(329, 380);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(162, 42);
            this.buttonBack.TabIndex = 8;
            this.buttonBack.Text = "Back";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // listBoxInstructor
            // 
            this.listBoxInstructor.DataSource = this.assignedBindingSource;
            this.listBoxInstructor.DisplayMember = "instructor_username";
            this.listBoxInstructor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxInstructor.FormattingEnabled = true;
            this.listBoxInstructor.ItemHeight = 20;
            this.listBoxInstructor.Location = new System.Drawing.Point(566, 171);
            this.listBoxInstructor.Name = "listBoxInstructor";
            this.listBoxInstructor.Size = new System.Drawing.Size(120, 144);
            this.listBoxInstructor.TabIndex = 10;
            this.listBoxInstructor.ValueMember = "instructor_username";
            // 
            // assignedBindingSource
            // 
            this.assignedBindingSource.DataMember = "Assigned";
            this.assignedBindingSource.DataSource = this.assignedDataSet;
            // 
            // assignedDataSet
            // 
            this.assignedDataSet.DataSetName = "assignedDataSet";
            this.assignedDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(562, 147);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 20);
            this.label4.TabIndex = 11;
            this.label4.Text = "Instructor";
            // 
            // listBoxLicence
            // 
            this.listBoxLicence.DataSource = this.assignedBindingSource;
            this.listBoxLicence.DisplayMember = "licence";
            this.listBoxLicence.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxLicence.FormattingEnabled = true;
            this.listBoxLicence.ItemHeight = 20;
            this.listBoxLicence.Location = new System.Drawing.Point(417, 170);
            this.listBoxLicence.Name = "listBoxLicence";
            this.listBoxLicence.Size = new System.Drawing.Size(120, 144);
            this.listBoxLicence.TabIndex = 13;
            this.listBoxLicence.ValueMember = "licence";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(413, 148);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 20);
            this.label6.TabIndex = 14;
            this.label6.Text = "Vehicle Licence";
            // 
            // assignedTableAdapter
            // 
            this.assignedTableAdapter.ClearBeforeFill = true;
            // 
            // vehicleAssignment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Brown;
            this.ClientSize = new System.Drawing.Size(884, 461);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.listBoxLicence);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.listBoxInstructor);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.buttonAssign);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxVehicle);
            this.Controls.Add(this.comboBoxInstructor);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.titleLabel);
            this.Name = "vehicleAssignment";
            this.Text = "vehicleAssignment";
            this.Load += new System.EventHandler(this.vehicleAssignment_Load);
            ((System.ComponentModel.ISupportInitialize)(this.instructorBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instructorDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.carBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.assignedBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.assignedDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxInstructor;
        private instructorDataSet1 instructorDataSet1;
        private System.Windows.Forms.BindingSource instructorBindingSource;
        private instructorDataSet1TableAdapters.InstructorTableAdapter instructorTableAdapter;
        private System.Windows.Forms.ComboBox comboBoxVehicle;
        private vehicleDataSet vehicleDataSet;
        private System.Windows.Forms.BindingSource carBindingSource;
        private vehicleDataSetTableAdapters.CarTableAdapter carTableAdapter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonAssign;
        private System.Windows.Forms.Button buttonBack;
        private assignedDataSet assignedDataSet;
        private System.Windows.Forms.BindingSource assignedBindingSource;
        private assignedDataSetTableAdapters.AssignedTableAdapter assignedTableAdapter;
        private System.Windows.Forms.ListBox listBoxInstructor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox listBoxLicence;
        private System.Windows.Forms.Label label6;
    }
}