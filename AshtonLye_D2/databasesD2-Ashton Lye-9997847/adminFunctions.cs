﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace databasesD2_Ashton_Lye_9997847
{
    public partial class adminFunctions : Form
    {
        public adminFunctions()
        {
            InitializeComponent();
        }

        private void registerInstructorButton_Click(object sender, EventArgs e)
        {
            registerInstructorForm registerInstructorForm1 = new registerInstructorForm();
            registerInstructorForm1.Show();
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void scheduleInstructorButton_Click(object sender, EventArgs e)
        {
            var userName = " ";
            var origin = "adminFunctions";
            instructorAvailability instructorAvailability1 = new instructorAvailability(userName, origin);
            instructorAvailability1.Show();
        }

        private void scheduleVehicleButton_Click(object sender, EventArgs e)
        {
            vehicleAssignment vehicleAssignment1 = new vehicleAssignment();
            vehicleAssignment1.Show();
        }
    }
}
