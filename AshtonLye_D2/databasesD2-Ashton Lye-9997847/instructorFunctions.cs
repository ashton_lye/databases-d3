﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace databasesD2_Ashton_Lye_9997847
{
    public partial class instructorFunctions : Form
    {
        private static string us2;

        public instructorFunctions(string userName)
        {
            InitializeComponent();
            us2 = userName;
        }

        private void bookLeaveButton_Click(object sender, EventArgs e)
        {
            var origin = "instructorFunctions";
            instructorAvailability instructorAvailability1 = new instructorAvailability(us2, origin);
            instructorAvailability1.Show();
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
