﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace databasesD2_Ashton_Lye_9997847
{
    public partial class registerInstructorForm : Form
    {
        public registerInstructorForm()
        {
            InitializeComponent();
        }

        private void buttonRegisterBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonRegisterInstructor_Click(object sender, EventArgs e)
        {
            var instructors = instructorDataSet1.Instructor.Rows.Count;
            var count = instructors;

            try
            {
                DataRow newInstructorRow = instructorDataSet1.Tables["Instructor"].NewRow();

                newInstructorRow["Instructor_username"] = instructor_usernameTextBox.Text;
                newInstructorRow["password"] = passwordTextBox.Text;
                newInstructorRow["sname"] = snameTextBox.Text;
                newInstructorRow["fname"] = fnameTextBox.Text;
                newInstructorRow["email"] = emailTextBox.Text;
                newInstructorRow["phone"] = phoneTextBox.Text;

                instructorDataSet1.Tables["Instructor"].Rows.Add(newInstructorRow); instructorTableAdapter.Update(instructorDataSet1);
                count++;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please enter valid information - E.G no letters in your phone number.");
            }

            if (count > instructors)
            {
                MessageBox.Show("Instructor Registered Successfully!");
            }
            else
            {
                MessageBox.Show("Registration Unsuccessful :(");
            }
        }

        private void registerInstructorForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'instructorDataSet1.Instructor' table. You can move, or remove it, as needed.
            this.instructorTableAdapter.Fill(this.instructorDataSet1.Instructor);

        }
    }
}
