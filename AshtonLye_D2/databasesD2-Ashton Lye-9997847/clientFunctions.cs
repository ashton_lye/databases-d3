﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace databasesD2_Ashton_Lye_9997847
{
    public partial class clientFunctions : Form
    {
        private static string us1;

        public clientFunctions(string userName)
        {
            InitializeComponent();
            us1 = userName;
            welcomeLabel.Text = $"Welcome, {us1}!";
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bookSessionButton_Click(object sender, EventArgs e)
        {
            bookSession bookSession1 = new bookSession(us1);
            bookSession1.Show();
        }
    }
}
