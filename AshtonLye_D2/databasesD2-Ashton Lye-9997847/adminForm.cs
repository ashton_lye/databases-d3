﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace databasesD2_Ashton_Lye_9997847
{
    public partial class adminForm : Form
    {
        public adminForm()
        {
            InitializeComponent();
        }

        private void adminForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'adminDataSet.Admin' table. You can move, or remove it, as needed.
            this.adminTableAdapter.Fill(this.adminDataSet.Admin);

        }

        private void buttonAdminBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonAdminLogin_Click(object sender, EventArgs e)
        {
            //Code that runs when the login button on the admin page is clicked
            var userName = textBoxAdminUsername.Text.Trim();
            var password = textBoxAdminPassword.Text.Trim();

            if (userName == "" || password == "")
            {
                MessageBox.Show("Please enter a valid username and password");
            }
            else
            {
                try
                {
                    adminTableAdapter.FillByMatchCredentials(adminDataSet.Admin, userName, password);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Please enter a valid username and password");
                }
                if (adminDataSet.Admin.Rows.Count == 1)
                {
                    MessageBox.Show("Logged in Successfully!");

                    adminFunctions adminFunctions1 = new adminFunctions();
                    adminFunctions1.Show();
                }
                else
                {
                    MessageBox.Show("No user found with those credentials - Please ensure you're entering them correctly!");
                }
            }
        }

        private void textBoxAdminUsername_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
