﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace databasesD2_Ashton_Lye_9997847
{
    public partial class clientForm : Form
    {
        public clientForm()
        {
            InitializeComponent();
        }

        private void clientForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'clientDataSet.Client' table. You can move, or remove it, as needed.
            this.clientTableAdapter.Fill(this.clientDataSet.Client);

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonClientLogin_Click(object sender, EventArgs e)
        {
            //Code that runs when the login button on the client page is clicked
            var userName = textBoxClientUsername.Text.Trim();
            var password = textBoxClientPassword.Text.Trim();

            if (userName == "" || password == "")
            {
                MessageBox.Show("Please enter a valid username and password");
            }
            else
            {
                try
                {
                    clientTableAdapter.FillByMatchCredentials(clientDataSet.Client, userName, password);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Please enter a valid username and password");
                }
                if (clientDataSet.Client.Rows.Count == 1)
                {
                    MessageBox.Show("Logged in Successfully!");

                    clientFunctions clientFunctions1 = new clientFunctions(userName);
                    clientFunctions1.Show();
                }
                else
                {
                    MessageBox.Show("No user found with those credentials - Please ensure you're entering them correctly!");
                }
            }
        }

        private void buttonClientRegister_Click(object sender, EventArgs e)
        {
            registerForm registerForm1 = new registerForm();
            registerForm1.Show();
        }
    }
}
