﻿namespace databasesD2_Ashton_Lye_9997847
{
    partial class adminFunctions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.titleLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.registerInstructorButton = new System.Windows.Forms.Button();
            this.scheduleInstructorButton = new System.Windows.Forms.Button();
            this.scheduleVehicleButton = new System.Windows.Forms.Button();
            this.logoutButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.Location = new System.Drawing.Point(97, 10);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(690, 67);
            this.titleLabel.TabIndex = 1;
            this.titleLabel.Text = "Driving Instruction Academy";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(284, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(292, 33);
            this.label1.TabIndex = 2;
            this.label1.Text = "Administrative Functions";
            // 
            // registerInstructorButton
            // 
            this.registerInstructorButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.registerInstructorButton.Location = new System.Drawing.Point(305, 134);
            this.registerInstructorButton.Name = "registerInstructorButton";
            this.registerInstructorButton.Size = new System.Drawing.Size(241, 52);
            this.registerInstructorButton.TabIndex = 3;
            this.registerInstructorButton.Text = "Register a New Instructor";
            this.registerInstructorButton.UseVisualStyleBackColor = true;
            this.registerInstructorButton.Click += new System.EventHandler(this.registerInstructorButton_Click);
            // 
            // scheduleInstructorButton
            // 
            this.scheduleInstructorButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scheduleInstructorButton.Location = new System.Drawing.Point(305, 206);
            this.scheduleInstructorButton.Name = "scheduleInstructorButton";
            this.scheduleInstructorButton.Size = new System.Drawing.Size(241, 52);
            this.scheduleInstructorButton.TabIndex = 4;
            this.scheduleInstructorButton.Text = "Schedule Instructor Availability";
            this.scheduleInstructorButton.UseVisualStyleBackColor = true;
            this.scheduleInstructorButton.Click += new System.EventHandler(this.scheduleInstructorButton_Click);
            // 
            // scheduleVehicleButton
            // 
            this.scheduleVehicleButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scheduleVehicleButton.Location = new System.Drawing.Point(305, 279);
            this.scheduleVehicleButton.Name = "scheduleVehicleButton";
            this.scheduleVehicleButton.Size = new System.Drawing.Size(241, 52);
            this.scheduleVehicleButton.TabIndex = 5;
            this.scheduleVehicleButton.Text = "Assign Vehicles";
            this.scheduleVehicleButton.UseVisualStyleBackColor = true;
            this.scheduleVehicleButton.Click += new System.EventHandler(this.scheduleVehicleButton_Click);
            // 
            // logoutButton
            // 
            this.logoutButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logoutButton.Location = new System.Drawing.Point(358, 353);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(141, 52);
            this.logoutButton.TabIndex = 6;
            this.logoutButton.Text = "Logout";
            this.logoutButton.UseVisualStyleBackColor = true;
            this.logoutButton.Click += new System.EventHandler(this.logoutButton_Click);
            // 
            // adminFunctions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Brown;
            this.ClientSize = new System.Drawing.Size(884, 461);
            this.Controls.Add(this.logoutButton);
            this.Controls.Add(this.scheduleVehicleButton);
            this.Controls.Add(this.scheduleInstructorButton);
            this.Controls.Add(this.registerInstructorButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.titleLabel);
            this.Name = "adminFunctions";
            this.Text = "adminFunctions";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button registerInstructorButton;
        private System.Windows.Forms.Button scheduleInstructorButton;
        private System.Windows.Forms.Button scheduleVehicleButton;
        private System.Windows.Forms.Button logoutButton;
    }
}